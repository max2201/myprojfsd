export default {
        en: {
            message: {
                allTasks: 'All Tasks',
                UnfinishedTasks: 'Unfinished Tasks',
                FinishedTasks: 'Finished Tasks',
                delete: 'Delete'
            }
        },
        ru: {
            message: {
                allTasks: 'Все задачи',
                UnfinishedTasks: 'Незавершённые задачи',
                FinishedTasks: 'Завершённые задачи',
                delete: 'Удалить'
            }
        }
}