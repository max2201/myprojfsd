import { defineStore } from "pinia";
import { ref } from "vue";

export const useRadio = defineStore("radio", () => {
  const activeValue = ref("");

  function setActive(val){
    activeValue.value = val
  }
  return { activeValue, setActive };
});
