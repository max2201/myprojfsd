export interface Task {
    id?: number
    title: string;
    description: string;
    date?: string[];
    date_start: string | null;
    date_end: string | null;
    date_time?: string | null;
    checked: boolean;
}
export interface TaskCardItem extends Task {
    date_start: string;
    date_end: string;
    date_time: string;
}
export type Pages = {
    [key: number]: string;
}