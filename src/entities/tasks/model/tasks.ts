import {defineStore} from "pinia";
import {reactive, ref} from "vue";
import {Task, TaskCardItem} from "../../../shared/api";

export const useTaskStore = defineStore("tasks", () => {
    const allItems = ref<Task[]>([{
        id: 12,
        title: 'asdad',
        description: 'asfafafsafxcvcbfgbfbfgb',
        date: 'asdad',
        date_start: null,
        date_end: null,
        date_time: null,
        checked: true,
    }, {
        id: 123,
        title: 'fdfs',
        description: 'sddddddddd',
        date: 's',
        date_start: null,
        date_end: null,
        date_time: null,
        checked: false,
    }, {
        id: 23,
        title: 'ff',
        description: 'aaaaaaaaaaaaaaaaaaaa',
        date: 's',
        date_start: null,
        date_end: null,
        date_time: null,
        checked: false,
    }]);

    const listItems = ref<Task[]>([]);

    let taskItem = reactive<TaskCardItem>({
        id: null,
        title: "",
        description: "",
        date_start: "",
        date_end: "",
        checked: false,
        date_time: "",
    });

    const loading = ref(false);

    async function changeTaskChecked(id: number) {
        const taskIndex = allItems.value.findIndex(task => task.id === id)
        allItems.value[taskIndex].checked = !allItems.value[taskIndex].checked
    }

    async function deleteTask(id: number) {
        const taskIndex = allItems.value.findIndex(task => task.id === id)
        allItems.value.splice(taskIndex, 1)
    }

    function getTaskList() {
        return listItems.value = allItems.value
    }

    function getFinishedTaskList() {
        listItems.value = allItems.value.filter(elem => {
            return elem.checked
        })
    }

    function getUnFinishedTaskList() {
        listItems.value = allItems.value.filter(elem => {
            return !elem.checked
        })
    }

    async function getTaskById(id: number) {
        let item = allItems.value.find(task => task.id === id)
        taskItem.title = item.title;
        taskItem.id = item.id;
        taskItem.description = item.description;
        taskItem.date_start = item.date_start;
        taskItem.date_end = item.date_end;
        taskItem.checked = item.checked;
        taskItem.date_time = item.date_time;
    }

    return {
        listItems,
        taskItem,
        getTaskList,
        getFinishedTaskList,
        getUnFinishedTaskList,
        getTaskById,
        loading,
        changeTaskChecked,
        deleteTask
    };
});
