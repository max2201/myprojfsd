export const routes = [
    {
        path: '/:locale(ru|en)?',
        props:true,
        children: [
            { path: "", component: () => import("./TasksListPage/index.vue"), name: "Home", },
            {
                path: "finished",
                component: () => import("./FinishedTasks/index.vue"),
                name: "Finished Tasks",
            },
            {
                path: "unfinished",
                component: () => import("./UnFinishedTasks/index.vue"),
                name: "Unfinished Tasks",
            },
            {
                path: "task/:id",
                component: () => import("./TaskPage/index.vue"),
                name: "Task Details"
            },
        ]
    },
    // {
    //     path: '*',
    //     redirect: { name: '404' }
    // },
];
