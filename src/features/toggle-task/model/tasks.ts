import { defineStore } from "pinia";
import { useRoute } from "vue-router";
import { taskModel } from "../../../entities/tasks/model";

export const useFeatureCheckedTaskStore = defineStore(
  "tasksCheckedFeature",
  () => {
    const taskStore = taskModel();
    const route = useRoute();

    async function checkedTask(checkedTask: any) {
          taskStore.changeTaskChecked(checkedTask.id)

        if (route.path === "/") {
          await taskStore.getTaskList();
        } else if (route.path === "/finished") {
          await taskStore.getFinishedTaskList();
        } else if (route.path.includes("task")) {
          await taskStore.getTaskById(+route.params.id);
        } else {
          await taskStore.getUnFinishedTaskList();
        }
    }
    return { checkedTask };
  }
);
