import { useRoute, useRouter } from "vue-router";
import { defineStore } from "pinia";
import { taskModel } from "../../../entities/tasks/model";

export const useFeatureDeleteTaskStore = defineStore(
  "tasksDeleteFeature",
  () => {
    const taskStore = taskModel();
    const route = useRoute();
    const router = useRouter();

    async function deleteTask(id: number | undefined) {
        taskStore.deleteTask(id)

        if (route.path === "/") {
            await taskStore.getTaskList();
        } else if (route.path === "/finished") {
            await taskStore.getFinishedTaskList();
        } else if (route.path.includes("task")) {
            await router.push("/");
        } else {
            await taskStore.getUnFinishedTaskList();
        }
    }

    return { deleteTask };
  }
);
