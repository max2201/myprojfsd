import {createI18n} from "vue-i18n";
import translations from "../../shared/translations"

export const i18n = createI18n({
    locale: 'ru',
    fallbackLocale: 'ru',
    globalInjection: true,
    messages:translations,
});