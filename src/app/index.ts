import {createApp} from "vue";
import {router, store, i18n} from "./providers";
import ElementPlus from 'element-plus'

import App from "./index.vue";

const initializeApp = createApp(App).use(router).use(store).use(ElementPlus).use(i18n);
export const app = initializeApp